# Sprint 2

<img src="/Imagens_Sprint2/logo.jpg"/>

## :computer: Proposta

Em continuação, nesta segunda sprint foi realizado o método de inserir,lista e remover atividades do planning,criação do sistema de equipes com persistencia de dados e funcionamento da tela de cadastro do dev através de link/e-mail,com melhora na autenticação dos usuários e Melhoria nos designs das páginas (Login, tela inicial do Scrum Master, cadastro de equipe, inserção e listagem de atividades).

##  :chart_with_upwards_trend: Meta da Sprint

Definimos como meta da Sprint 2, a continuação dos requisitos desenvolvidos da Sprint 1, com um objetivo de iniciar o desenvolvimento do front end, um cadastro básico para scrum e o desenvolvedor; e do back end uma tela de votação do sistema planning pokker;


## :dart: Detalhes da Sprint:

**Implementações do back-end:**

- Ajustes na autenticação dos usuários;
- Cadastro do usaurio por link/ e-mail;
- Telas de Login, tela inicial do Scrum Master, cadastro de equipe, inserção e listagem de atividades atualizadas para melhor funcionamento;

**Implementações Front-End:**
- Criação do sistema de equipe com persistência de dados;
- Funcionamento do sistema de Inserir, listar e remover atividades do Planning;


## Apresentação da Sprint
<a href="https://docs.google.com/presentation/d/1LEb0g76bfm00sGO6-cwRq1JQ2uwIqmw8/edit#slide=id.p35">Apresentação PowerPoint</a>



## Wireframes:



<img src="/Imagens_Sprint2/PP_Cad_Equipe.png"/>

<img src="/Imagens_Sprint2/Tela_login_Sprint2.jpeg"/>

<img src="/Imagens_Sprint2/Tela_Planning_Sprint_2.jpeg"/>

## :department_store: Proposta para a Proxima Sprint:
- Para a próxima sprint teremos o objetivo de desenvolver o sistema votação de atividades e itens a ser avaliadas e sistemade criação de indicadores do andamento de votação de determinadas atividades.
